package com.lucy.youtaro_sr_18;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YoutaroSr18Application {

    public static void main(String[] args) {
        SpringApplication.run(YoutaroSr18Application.class, args);
    }

}
