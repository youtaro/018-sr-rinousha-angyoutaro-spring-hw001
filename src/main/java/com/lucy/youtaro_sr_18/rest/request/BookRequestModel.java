package com.lucy.youtaro_sr_18.rest.request;

import com.lucy.youtaro_sr_18.repo.model.CategoryModel;

public class BookRequestModel {
    private int id;
    private String title;
    private String description;
    private String author;
    private String thumbnail;
    CategoryModel category;

    public BookRequestModel(int id, String title, String description, String author, String thumbnail, CategoryModel category) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.author = author;
        this.thumbnail = thumbnail;
        this.category = category;
    }

    public BookRequestModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public CategoryModel getCategory() {
        return category;
    }

    public void setCategory(CategoryModel category) {
        this.category = category;
    }
}
