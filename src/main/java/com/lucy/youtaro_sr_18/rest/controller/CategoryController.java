package com.lucy.youtaro_sr_18.rest.controller;

import com.lucy.youtaro_sr_18.Service.imp.CategoryServiceImpl;
import com.lucy.youtaro_sr_18.repo.model.BookModel;
import com.lucy.youtaro_sr_18.repo.model.CategoryModel;
import com.lucy.youtaro_sr_18.rest.request.CategoryRequestModel;
import com.lucy.youtaro_sr_18.rest.response.BaseApiResponse;
import com.lucy.youtaro_sr_18.rest.response.GetApiResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController

public class CategoryController {
    private CategoryServiceImpl categoryService;

    @Autowired
    public void setCategoryService(CategoryServiceImpl categoryService) {
        this.categoryService = categoryService;
    }

    //find all dem books
    @GetMapping("/category")
    public ResponseEntity<GetApiResponse<List<CategoryModel>>> findAll(){
        ModelMapper mapper = new ModelMapper();
        GetApiResponse<List<CategoryModel>> response =
                new GetApiResponse<>();

        List<CategoryModel> categoryDtoList = categoryService.findAll();

        response.setData(categoryDtoList);
        response.setStatus(HttpStatus.OK);
        response.setMessage("Get All Datas");
        return ResponseEntity.ok(response);

    }

    @GetMapping("/category/{categoryId}")
    public ResponseEntity<GetApiResponse<CategoryModel>> findById(@PathVariable int categoryId){
        GetApiResponse<CategoryModel> response = new GetApiResponse<>();
        CategoryModel bookDto = categoryService.findById(categoryId);

        response.setData(bookDto);
        response.setStatus(HttpStatus.OK);
        response.setMessage("Found");
        return ResponseEntity.ok(response);
    }
    //
    @PostMapping("/category")
    public ResponseEntity<BaseApiResponse<CategoryRequestModel>> insert(@RequestBody CategoryRequestModel book){

        BaseApiResponse<CategoryRequestModel> response = new BaseApiResponse<>();

        ModelMapper modelMapper = new ModelMapper();
        CategoryModel categoryDto = modelMapper.map(book, CategoryModel.class);
        categoryService.insert(categoryDto);

        response.setMessage("Found");
        response.setStatus(HttpStatus.OK);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/category/{categoryId}")
    public ResponseEntity<BaseApiResponse<CategoryRequestModel>> deleteById(@PathVariable int categoryId){
        BaseApiResponse<CategoryRequestModel> res = new BaseApiResponse<>();
        categoryService.deleteById(categoryId);
        res.setMessage("Deleted!");
        res.setStatus(HttpStatus.OK);
        return ResponseEntity.ok(res);
    }

    @PutMapping("/category/{categoryId}")
    public ResponseEntity<BaseApiResponse<String>> update(@RequestBody  CategoryRequestModel categoryRequestModel, @PathVariable("categoryId") Integer id)
    {
        BaseApiResponse<String> response = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();
        CategoryModel category = mapper.map(categoryRequestModel, CategoryModel.class);
        categoryService.update(category,id);
        response.setStatus(HttpStatus.OK);
        response.setMessage("Updated");
        return ResponseEntity.ok(response);
    }
}
