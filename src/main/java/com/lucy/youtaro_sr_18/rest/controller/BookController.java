package com.lucy.youtaro_sr_18.rest.controller;

import com.lucy.youtaro_sr_18.Service.imp.BookServiceImpl;
import com.lucy.youtaro_sr_18.repo.model.BookModel;
import com.lucy.youtaro_sr_18.rest.request.BookRequestModel;
import com.lucy.youtaro_sr_18.rest.response.BaseApiResponse;
import com.lucy.youtaro_sr_18.rest.response.GetApiResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("book")
@RestController
public class BookController {
    private BookServiceImpl bookService;

    @Autowired
    public void setBookService(BookServiceImpl bookService) {
        this.bookService = bookService;
    }

    @GetMapping
    public ResponseEntity<GetApiResponse<List<BookModel>>> findAll() {
        GetApiResponse<List<BookModel>> response =
                new GetApiResponse<>();

        List<BookModel> bookDtoList = bookService.findAll();

        response.setData(bookDtoList);
        response.setStatus(HttpStatus.OK);
        response.setMessage("Get All Datas");
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{bookId}")
    public ResponseEntity<GetApiResponse<BookModel>> findById(@PathVariable int bookId) {
        GetApiResponse<BookModel> response = new GetApiResponse<>();

        BookModel bookDto = bookService.findById(bookId);

        response.setData(bookDto);
        response.setStatus(HttpStatus.OK);
        response.setMessage("Found");
        return ResponseEntity.ok(response);
    }

    @GetMapping(params = "title")
    public ResponseEntity<GetApiResponse<List<BookModel>>> findTitle(@RequestParam String title) {
        GetApiResponse<List<BookModel>> response = new GetApiResponse<>();

        List<BookModel> bookDto = bookService.findTitle(title);

        response.setData(bookDto);
        response.setMessage("Found");
        response.setStatus(HttpStatus.OK);
        return ResponseEntity.ok(response);
    }

    @GetMapping(params = {"title","categoryId"})
    public ResponseEntity<GetApiResponse<List<BookModel>>> findBoth(
            @RequestParam(value="title") String title,
            @RequestParam(value="categoryId") int id
    ) {
        GetApiResponse<List<BookModel>> response = new GetApiResponse<>();

        List<BookModel> bookDto = bookService.findBoth(id,title);

        response.setData(bookDto);
        response.setMessage("Found");
        response.setStatus(HttpStatus.OK);
        return ResponseEntity.ok(response);
    }

    @GetMapping(params = "categoryId")
    public ResponseEntity<GetApiResponse<List<BookModel>>> findCategoryId(@RequestParam int categoryId) {
        GetApiResponse<List<BookModel>> response = new GetApiResponse<>();

        List<BookModel> bookDto = bookService.findCategoryId(categoryId);

        response.setData(bookDto);
        response.setMessage("Found");
        response.setStatus(HttpStatus.OK);
        return ResponseEntity.ok(response);
    }

    @PostMapping
    public ResponseEntity<BaseApiResponse<BookRequestModel>> insert(@RequestBody BookRequestModel book) {

        BaseApiResponse<BookRequestModel> res = new BaseApiResponse<>();

        ModelMapper modelMapper = new ModelMapper();
        BookModel bookDto = modelMapper.map(book, BookModel.class);
        bookService.insert(bookDto);

        res.setStatus(HttpStatus.OK);
        res.setMessage("Added");
        return ResponseEntity.ok(res);

    }

    @DeleteMapping("/{bookId}")
    public ResponseEntity<BaseApiResponse<BookRequestModel>> deleteById(@PathVariable int bookId) {
        BaseApiResponse<BookRequestModel> res = new BaseApiResponse<>();
        bookService.deleteById(bookId);
        res.setMessage("Deleted!");
        res.setStatus(HttpStatus.OK);

        return ResponseEntity.ok(res);
    }

    @PutMapping("/{bookId}")
    public ResponseEntity<BaseApiResponse<String>> update(@RequestBody BookRequestModel bookRequestModel, @PathVariable("bookId") int id)
    {
        BaseApiResponse<String> response = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();
        BookModel book = mapper.map(bookRequestModel, BookModel.class);
        bookService.update(book,id);
        response.setStatus(HttpStatus.OK);
        response.setMessage("Updated");
        return ResponseEntity.ok(response);
    }
}
