package com.lucy.youtaro_sr_18.rest.response;


import org.springframework.http.HttpStatus;

public class BaseApiResponse<T> {
    private HttpStatus status;
    private String message;

    public BaseApiResponse(){}

    public BaseApiResponse(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
