package com.lucy.youtaro_sr_18.repo;

import com.lucy.youtaro_sr_18.repo.model.CategoryModel;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepo {

    @Select("select * from tb_categories ORDER BY id asc")
    List<CategoryModel> findAll();

    @Select("SELECT * FROM tb_categories WHERE id = #{id}")
    CategoryModel findById(int id);

    @Insert("INSERT INTO tb_categories (title) " +
            "VALUES (#{title})")
    boolean insert(CategoryModel categoryDto);

    @Delete("DELETE FROM tb_categories WHERE id = #{id}")
    boolean delete(int id);

    @Update("UPDATE tb_categories SET title=#{category.title} WHERE id=#{id}")
    int update(CategoryModel category,int id);
}
