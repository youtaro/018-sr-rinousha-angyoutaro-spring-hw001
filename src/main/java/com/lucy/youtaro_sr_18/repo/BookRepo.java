package com.lucy.youtaro_sr_18.repo;

import com.lucy.youtaro_sr_18.repo.model.BookModel;
import com.lucy.youtaro_sr_18.repo.model.CategoryModel;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepo {

    @Select("SELECT * FROM tb_categories WHERE id=#{category_id}")
    CategoryModel getCategory(int category_id);

    @Select("SELECT * FROM tb_books ORDER BY id asc")
    @Results({
            @Result(
                    property = "category",
                    javaType = CategoryModel.class,
                    column = "category_id",
                    one = @One(select = "getCategory")
            ),
    })
    List<BookModel> findAll();


    @Select("SELECT * FROM tb_books WHERE id = #{id}")
    @Results({
            @Result(
                    property = "category",
                    javaType = CategoryModel.class,
                    column = "category_id",
                    one = @One(select = "getCategory")
            ),
    })
    BookModel findById(int id);

    @Select("SELECT * FROM tb_books WHERE title = #{title}")
    @Results({
            @Result(
                    property = "category",
                    javaType = CategoryModel.class,
                    column = "category_id",
                    one = @One(select = "getCategory")
            ),
    })
    List<BookModel> findTitle(String title);

    @Select("SELECT * FROM tb_books WHERE category_id = #{id} ")
    @Results({
            @Result(
                    property = "category",
                    javaType = CategoryModel.class,
                    column = "category_id",
                    one = @One(select = "getCategory")
            ),
    })
    List<BookModel> findCategoryId(int id);

    @Select("SELECT * FROM tb_books WHERE category_id = #{id} and title = #{title}")
    @Results({
            @Result(
                    property = "category",
                    javaType = CategoryModel.class,
                    column = "category_id",
                    one = @One(select = "getCategory")
            ),
    })
    List<BookModel> findBoth(int id , String title);


    @Insert("INSERT INTO tb_books (title, description, author, thumbnail,category_id) " +
            "VALUES (#{title}, #{description}, #{author}, #{thumbnail}, #{category.id})")
    boolean insert(BookModel bookDto);

    @Delete("DELETE FROM tb_books WHERE id = #{id}")
    boolean delete(int id);

    @Update("UPDATE tb_books SET title=#{book.title},author=#{book.author},description=#{book.description},category_id=#{book.category.id} WHERE id=#{id}")
    int update(BookModel book,int id);
}
