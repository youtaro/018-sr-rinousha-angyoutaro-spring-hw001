package com.lucy.youtaro_sr_18.Service;

import com.lucy.youtaro_sr_18.repo.model.BookModel;

import java.util.List;

public interface BookService {
    BookModel insert(BookModel bookDto);
    List<BookModel> findAll();
    BookModel findById(int id);
    List<BookModel> findTitle(String title);
    List<BookModel> findCategoryId(int id);
    List<BookModel> findBoth(int id,String title);
    boolean deleteById(int id);
    int update(BookModel book, int id);

}