package com.lucy.youtaro_sr_18.Service.imp;

import com.lucy.youtaro_sr_18.Service.BookService;
import com.lucy.youtaro_sr_18.repo.BookRepo;
import com.lucy.youtaro_sr_18.repo.model.BookModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {
    private BookRepo bookRepo;

    @Autowired
    public void setBookRepo(BookRepo bookRepo) {
        this.bookRepo = bookRepo;
    }

    @Override
    public BookModel insert(BookModel bookDto) {
        boolean isInserted = bookRepo.insert(bookDto);
        if(isInserted){
            return bookDto;
        } else {
            return null;
        }
    }

    @Override
    public List<BookModel> findAll() {
        return bookRepo.findAll();
    }

    @Override
    public BookModel findById(int id) {
        return bookRepo.findById(id);

    }

    @Override
    public List<BookModel> findTitle(String title) {
        return bookRepo.findTitle(title);

    }

    @Override
    public List<BookModel> findCategoryId(int id) {
        return bookRepo.findCategoryId(id);

    }

    @Override
    public List<BookModel> findBoth(int id, String title) {
        return bookRepo.findBoth(id,title);

    }

    @Override
    public boolean deleteById(int id) {
        return bookRepo.delete(id);

    }

    @Override
    public int update(BookModel book, int id) {
        return bookRepo.update(book,id);

    }
}
