package com.lucy.youtaro_sr_18.Service.imp;

import com.lucy.youtaro_sr_18.Service.CategoryService;
import com.lucy.youtaro_sr_18.repo.CategoryRepo;
import com.lucy.youtaro_sr_18.repo.model.CategoryModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepo categoryRepo;

    @Autowired
    public void setCategoryRepo(CategoryRepo categoryRepo) {
        this.categoryRepo = categoryRepo;
    }

    @Override
    public CategoryModel insert(CategoryModel categoryDto) {
        boolean isInserted = categoryRepo.insert(categoryDto);
        if(isInserted) {
            return categoryDto;
        }else {
            return null;
        }
    }

    @Override
    public List<CategoryModel> findAll() {
        return categoryRepo.findAll();
    }

    @Override
    public CategoryModel findById(int id) {
        return categoryRepo.findById(id);
    }

    @Override
    public boolean deleteById(int id) {
        return categoryRepo.delete(id);
    }

    @Override
    public int update(CategoryModel category, int id) {
        return categoryRepo.update(category,id);
    }
}
