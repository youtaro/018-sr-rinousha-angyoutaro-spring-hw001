package com.lucy.youtaro_sr_18.Service;

import com.lucy.youtaro_sr_18.repo.model.CategoryModel;

import java.util.List;

public interface CategoryService {
    CategoryModel insert(CategoryModel categoryDto);
    List<CategoryModel> findAll();
    CategoryModel findById(int id);
    boolean deleteById(int id);
    int update(CategoryModel category,int id);

}
